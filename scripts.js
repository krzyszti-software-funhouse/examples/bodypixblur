const video = document.getElementById('video');
const canvas = document.getElementById('output');

// Effect settings
const backgroundBlurAmount = 3;
const edgeBlurAmount = 3;
const flipHorizontally = false;

// BodyPix net
let net = null;

// Controll the effect in html with button
let blur_video = true;

function flipBlur() {
  blur_video = !blur_video;
}

async function getMedia() {
  navigator.getUserMedia = navigator.getUserMedia ||
    navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
}

async function checkMedia(){
  if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
    throw new Error(
      'Browser API navigator.mediaDevices.getUserMedia not available');
  }
}

async function setVideoSource(){
  video.srcObject = await navigator.mediaDevices.getUserMedia({'audio': false, 'video': true});
}

async function loadVideo() {
  await getMedia();
  await checkMedia();
  await setVideoSource();

  return new Promise((resolve) => {
    video.onloadedmetadata = () => {
      video.width = video.videoWidth;
      video.height = video.videoHeight;
      resolve(video);
    };
  });
}

// Initialize the BodyPix network
async function loadBodyPix() {
  net = await bodyPix.load({
    architecture: 'MobileNetV1',
    outputStride: 16,
    multiplier: 0.5,
    quantBytes: 2
  });
}


// Estimate the segmentation for what the video is right now.
async function estimateSegmentation() {
  return net.segmentPerson(video, {
    internalResolution: 'low',
    segmentationThreshold: 0.5,
    maxDetections: 5,
    scoreThreshold: 0.3,
    nmsRadius: 20,
  });
}

// Apply the effect in the real time
function segmentBodyInRealTime() {
  async function bodySegmentationFrame() {
    const multiPersonSegmentation = await estimateSegmentation();
    if (blur_video) {
      backgroundBlur = backgroundBlurAmount;
      edgeBlur = edgeBlurAmount;
    } else {
      backgroundBlur = 0;
      edgeBlur = 0;
    }
    bodyPix.drawBokehEffect(
      canvas, video, multiPersonSegmentation,
      backgroundBlur,
      edgeBlur, flipHorizontally);
    requestAnimationFrame(bodySegmentationFrame);
  }

  bodySegmentationFrame();
}


async function bindPage() {
  await loadBodyPix();
  await loadVideo();
  segmentBodyInRealTime();
}

bindPage();
